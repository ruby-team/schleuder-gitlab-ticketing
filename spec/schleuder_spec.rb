require 'spec_helper'
require 'schleuder/filters/post_decryption/99_gitlab_ticketing'

describe Schleuder::Filters do
  context 'with talking to gitlab' do
    let(:config){ SchleuderGitlabTicketing::Config.new('spec/fixtures/gitlab.yml') }
    let(:new_ticket) { double }
    let(:gitlab) { double }
    let(:list) { double }
    let(:mail) { MailStub.new('user@example.com','question') }
    before(:each) do
      # reset the mocked classes so they don't leak to other tests
      Schleuder::Filters.instance_variable_set('@gt_config',nil)
      allow(SchleuderGitlabTicketing::Config).to receive(:new).and_return(config)
      allow_any_instance_of(SchleuderGitlabTicketing::List).to receive(:gitlab).and_return(gitlab)
      list1_project = double
      allow(list1_project).to receive(:name).and_return('tickets')
      allow(list1_project).to receive(:id).and_return(123)
      allow(list1_project).to receive_message_chain(:namespace,:name).and_return('support')
      allow(gitlab).to receive(:search_projects).with('tickets').and_return([list1_project])
    end
    context 'with processing' do
      before(:each) do
        expect(list).to receive(:email).and_return('test')
        expect(gitlab).to receive(:create_issue).with(123,'question',{ description: "From: user@example.com\n\nMessage-Id: 1-abc-3\n\nSubject: question"}).and_return(new_ticket)
      end
      it 'creates a new ticket' do
        expect(new_ticket).to receive(:iid).and_return(42)
        expect(new_ticket).to receive(:state).and_return('open')
        expect(Schleuder::Filters.gitlab_ticketing(list, mail)).to be(nil)
        expect(mail.subject).to eql('question [gl/tickets#42]')
      end
      it 'works even on en exception' do
        expect(new_ticket).to receive(:iid).and_raise(Exception,'internal test error')
        logger = double
        expect(list).to receive(:logger).and_return(logger)
        expect(logger).to receive(:notify_admin)
        expect(Schleuder::Filters.gitlab_ticketing(list, mail)).to be(nil)
        expect(mail.subject).to eql('question')
      end
    end
    context 'subject filter' do
      let(:mail) { MailStub.new('user@example.com','ignore me') }
      it 'skips email if subject filter matches' do
        logger = double
        expect(list).to receive(:logger).and_return(logger)
        expect(logger).to receive(:debug).with('Email skipped by list configuration')
        expect(list).to receive(:email).and_return('test')
        expect(Schleuder::Filters.gitlab_ticketing(list, mail)).to be(nil)
        expect(mail.subject).to eql('ignore me')
      end
    end
    context 'sender filter' do
      let(:mail) { MailStub.new('test@example.com','like me') }
      it 'skips email if subject filter matches' do
        logger = double
        expect(list).to receive(:logger).and_return(logger)
        expect(logger).to receive(:debug).with('Email skipped by list configuration')
        expect(list).to receive(:email).and_return('test')
        expect(Schleuder::Filters.gitlab_ticketing(list, mail)).to be(nil)
        expect(mail.subject).to eql('like me')
      end
    end
    it 'skips non configured lists' do
      expect(list).to receive(:email).and_return('non-existing-test')
      logger = double
      expect(list).to receive(:logger).and_return(logger)
      expect(logger).to receive(:debug).with('No gitlab ticketing enabled for list')
      expect(Schleuder::Filters.gitlab_ticketing(list, mail)).to be(nil)
      expect(mail.subject).to eql('question')
    end
    it 'skips on wrongly configured lists' do
      expect(list).to receive(:email).and_return('invalid1')
      logger = double
      expect(list).to receive(:logger).and_return(logger)
      expect(logger).to receive(:error)
      expect(Schleuder::Filters.gitlab_ticketing(list, mail)).to be(nil)
      expect(mail.subject).to eql('question')
    end
  end
end

